<?php
    include_once('api/get.class.php');
    include_once('api/post.class.php');
    ini_set('display_errors', 1);


    $data = get::getAll();
?>

    <table class="striped responsive-table highlight centered">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nom d'hôte</th>
                <th>Client</th>
                <th>Nom de domaine</th>
                <th>Adresse IP</th>
                <th>VM Existante</th>
                <th>VM allumée</th>
                <th>DNS</th>
                <th>Service Web</th>
                <th>Monitoring</th>
                <th>Sauvegarde</th>
            </tr>

        <tbody>
            <?php
                foreach ($data as $row) {
                    echo '<tr>';

                    foreach($row as $doge){

                        if($doge == "Yes"){
                            echo '<td> <i class="fa-regular fa-circle-check"></i> </td>';
                        }elseif($doge == "No"){
                            echo '<td> <i class="fa-regular fa-circle-xmark"></i> </td>';
                        }else{
                            echo "<td> $doge </td>";
                        }
                    }
                    echo '</tr>';
                }
            ?>
        </tbody>
    </table>
