<?php

    class get{

        public static function getAll($csv=null){

            $csv = $csv ?? '/astagia/data/VMs.csv';
            $finalData = [];
            $handler = fopen($csv, 'r');
            fgetcsv($handler, 10000, ","); // Pour sauter la 1ère ligne
            while(($data = fgetcsv($handler)) !== false){
                $finalData[] = $data;
            }

            fclose($handler);
            return $finalData;
        }

        
        public static function getIP($dev, $choix=null){

            $data = get::getAll('/astagia-test/data/VMs.csv');
            foreach($data as $row){
                if($row[2] == $dev){
                    $ip = $row[3];
                    $exist = $row[4];
                }
            }
            if($choix == 'ip'){
                echo $ip;
                return $ip;
            } else{ 
                echo $exist;
            }
        } 
        
        public static function getPing($ip){
            $result = 1;
            exec("ping -c 4 $ip", $output, $result);
            while($result != 0){
                exec("ping -c 4 $ip", $output, $result);
            }
            echo "OK";
        }

        public static function getSuppresion($dev){
            shell_exec("ssh s-master@localhost /astagia-test/DeleteVM.sh $dev");
            echo "Suppression de la VM terminée !";
        }

        public static function getSuppresionSnap($dev){
            $hostname = "absdc-astagia-autoTest-".$dev;
            shell_exec("ssh s-master@localhost /astagia-test/Revert.sh $hostname delete");
            print_r("Snapshot supprimé !");
        }

        public static function getCreationSnap($dev){
            $hostname = "absdc-astagia-autoTest-".$dev;
            shell_exec("ssh s-master@localhost /astagia-test/Revert.sh $hostname create");
            print_r("Snapshot crée !");
        }
        
        public static function getUpdateSvn($ip){
            shell_exec("ssh s-terraform@".$ip." sudo svn_export -s yes");
        }

?>
