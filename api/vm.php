<?php
    include("post.class.php");
    include("get.class.php");
    $request_method = $_SERVER["REQUEST_METHOD"];

    switch($request_method){

        case 'POST':
            if(!empty($_POST)){
                print_r($_POST);
                http_response_code(200);
                echo json_encode("Création de la VM lancée");
                post::create($_POST["dev"], $_POST["revision"]);
            } else {
                print_r($_POST);
                http_response_code(405);
                echo json_encode("Post : Merci d'entrer le nom d'un dev et une révision");
            }
            break;
        case 'GET':
            if(!empty($_GET)){
                http_response_code(200);
                get::getIP($_GET["dev"], $_GET["choix"]);
            } else {
                http_response_code(405);
                print_r("GET : Merci d'entrer le nom d'un dev ainsi qu'une option");
            }
            break;
        default:
            header("HTTP/1.0 405 Method Not Allowed");
            break;
    }
?>