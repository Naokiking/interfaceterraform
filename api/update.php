<?php

    include("get.class.php");
    $request_method = $_SERVER["REQUEST_METHOD"];

    switch($request_method){
        case 'GET':
            if(!empty($_GET)){
                http_response_code(200);
                $ip = get::getIP($_GET['dev'], 'ip');
                get::getUpdateSvn($ip);
            }
            break;
        default:
            header("HTTP/1.0 405 Method Not Allowed");
            break;
    } 

?>