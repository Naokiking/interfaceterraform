<?php
    include_once('api/post.class.php');
    ini_set('display_errors', 1);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Import Materialize, Google Font, Font awesome  -->
    <script src="https://kit.fontawesome.com/e8dc6b7a78.js" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="css/images/terraform-favicon.png" />
    <link rel="stylesheet" href="css/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Hind+Siliguri&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>


    <title>Monitoring Terraform</title>

</head>
    <body>
        <main class="container">

            <center>
                <h1> Monitoring des machines terraform </h1>
            </center>

            <div class="card-panel post">
                <form action="" method="post" id="myForm">
                    <div class="input-field">
                        <input type="text" name="client" class="validate" id="client" required="required">
                        <label for="client"> Nom du client </label>
                    </div>
                    <div class="button-progress">
                        <button class="btn waves-effect waves-light pulse" type="submit"  name="create" > Créer ! </button>
                        <?php
                            if(isset($_POST['create'])){
                                post::post($_POST['client']);
                            }
                        ?>
                    </div>
                </form>
            </div>
            <br>
            <div class="card-panel log" id="log">
                
                <?php include('log.php'); ?>

            </div>
            <br>
            <div class="card-panel table" id="myTable">

                <?php include('listVM.php'); ?>

            </div>
        </main>
        <script src="script.js"></script>
    </body>
</html>
