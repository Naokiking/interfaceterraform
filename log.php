
<h5>Résultat de la dernière exécution :</h5>
<br>
<div class="teal-text lighten-2">
    <?php
    $file_log = explode("\n", shell_exec("cat '/var/www/astagia/output.txt'"));
    foreach ($file_log as $line_num => $line) {
        echo htmlspecialchars($line) . "<br />\n";
    }
    ?>
</div>
<h5>Log :</h5>
<br>
<div class="teal-text lighten-2">
    <?php
    $file_log = explode("\n", shell_exec("tail -5 '/astagia/log/automation.log'"));
    foreach ($file_log as $line_num => $line) {
        echo htmlspecialchars($line) . "<br />\n";
    }
    ?>
</div>